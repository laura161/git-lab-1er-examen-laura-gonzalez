using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager1 : MonoBehaviour
{
    public GameObject jugador;
    float TiempoRestante;
    public TMPro.TMP_Text TextoGameOver;
    //boll: valor boleano, puede ser false o true
    public bool SinTiempoRestante;
      

    void Start()
    {
        ComenzarJuego();
        TextoGameOver.text = "";
    }

    void Update()
    {
        if (TiempoRestante == 0)
        {
            ComenzarJuego();
        }

    }

    void ComenzarJuego()
    {
        jugador.transform.position = new Vector3(0f, 0f, 0f);
        StartCoroutine(ComenzarCronometro(60));
    }

    public IEnumerator ComenzarCronometro(float valorCronometro)
    {
        TiempoRestante = valorCronometro;

        while (TiempoRestante > 0)
        {
            Debug.Log("Restan " + TiempoRestante + " segundos.");
            yield return new WaitForSeconds(1.0f);
            TiempoRestante--;
        }

        if (TiempoRestante <= 0)
        {
            Time.timeScale = 0;
            TextoGameOver.text = "Game Over";   
        }
     
    }
    
}
