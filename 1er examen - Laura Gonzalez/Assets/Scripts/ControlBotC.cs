using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlBotC : MonoBehaviour
{

    bool tengoQueBajar = false;
    int rapidez = 2;

    void Update()
    {
        if (transform.position.y >= 5)
        {
            tengoQueBajar = true;
        }
        if (transform.position.y <= 1)
        {
            tengoQueBajar = false;
        }

        if (tengoQueBajar)
        {
            Bajar();
        }
        else
        {
            Subir();
        }

    }

    void Subir()
    {
        transform.position += transform.up * rapidez * Time.deltaTime;
    }

    void Bajar()
    {
        transform.position -= transform.up * rapidez * Time.deltaTime;
    }
}
