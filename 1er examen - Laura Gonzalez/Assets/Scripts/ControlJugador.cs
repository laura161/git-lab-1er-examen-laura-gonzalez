using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ControlJugador : MonoBehaviour
{
    public float VelocidadJugadorConHabilidad = 10.0f;
    public float rapidezDesplazamiento = 10.0f;
    private Rigidbody Rb;
    public float FuerzaDeSalto = 1.0f;
    private bool SobreElSuelo = true;
    public int MaximoSaltos = 2;
    public int SaltoActual = 0;
    float xInicial, yInicial;
    public TMPro.TMP_Text TextoGanaste;
    public TMPro.TMP_Text TextoRapidezYTama˝o;
    public TMPro.TMP_Text TextoCantidadRecolectados;
    private int contC;
    private int contH;
  



    void Start()
    {
        xInicial = transform.position.x;
        yInicial = transform.position.y;
        //cuando comience que desaparezca el cursor
        Cursor.lockState = CursorLockMode.Locked;
        Rb = GetComponent<Rigidbody>();
        contH = 0;
        contC = 0;
        TextoGanaste.text = "";
        TextoRapidezYTama˝o.text = "";
        setearTextos();

    }
    void Update()
    {
        //movimientos del teclado vertical ((1)w,(-1)s), horizontal ((-1)a,(1)d)
        float vertical = Input.GetAxis("Vertical") * rapidezDesplazamiento;
        float horizontal = Input.GetAxis("Horizontal") * rapidezDesplazamiento;

        //con esto en lugar de ser por f es por seg (timedeltatime m por seg)
        vertical *= Time.deltaTime;
        horizontal *= Time.deltaTime;

        //movimientos de los ejes
        transform.Translate(horizontal, 0, vertical);

        if (Input.GetKeyDown("escape"))
        {
            //cuando se aprete escape que aparezca el cursor
            Cursor.lockState = CursorLockMode.None;
        }
        //salto doble
        if (Input.GetKeyDown("space") && (SobreElSuelo || MaximoSaltos > SaltoActual))
        {
            Rb.AddForce(new Vector3(0, FuerzaDeSalto, 0), ForceMode.Impulse);
            SobreElSuelo = false;
            SaltoActual++;
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            Scene currentScene = SceneManager.GetActiveScene();
            SceneManager.LoadScene(currentScene.name);
        }
        

    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("coleccionable") == true)
        {
            contC = contC + 1;
            setearTextos();
            other.gameObject.SetActive(false);
        }
        
        if (other.gameObject.CompareTag("HabilidadColeccionable"))
        {
            contH = contH + 1;
            setearTextos();
            other.gameObject.SetActive(false);
        }
        
        if (other.gameObject.CompareTag("HabilidadColeccionable"))
        {
            this.transform.localScale = new Vector3( 2f, 2f, 2f);
            rapidezDesplazamiento *= 2;
            other.gameObject.SetActive(false);
        }
        
        if (other.gameObject.CompareTag("Suelo"))
        {
            Scene currentScene = SceneManager.GetActiveScene();
            SceneManager.LoadScene(currentScene.name);
        }
        
        if (other.gameObject.CompareTag("VuelveAEmpezar"))
        {
            Scene currentScene = SceneManager.GetActiveScene();
            SceneManager.LoadScene(currentScene.name);
        }
      
    }

    //para que vuelva a saltar cuando toca el piso
    private void OnCollisionEnter(Collision collision)
    {
        SobreElSuelo = true;
        SaltoActual = 0;
    }
   
    private void setearTextos()
    {
        TextoCantidadRecolectados.text = "Cantidad recolectados: " + contC.ToString() + "/8";
        
        if (contC >=8)
        {
            Time.timeScale = 0;
            TextoGanaste.text = "Ganaste!";
        }
        if (contH >= 1)
        {
            TextoRapidezYTama˝o.text = "Te queda poco tiempo";
        }
    }


}
